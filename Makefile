# ============================================================================
# 	Project : Similarity 3D
# 	Author  : Jeremy Steward
# 	Date    : 2017-07-31
# ============================================================================

CC       = gcc
CXX      = g++
CFLAGS   = -std=c11 -pedantic -Wall -Wextra -g
CXXFLAGS = -std=c++14 -pedantic -Wall -Wextra -g
EIGEN3_INCLUDE_DIR ?= 'D:\Programs\eigen-eigen-5a0156e40feb'
EXTFLAGS = -I $(EIGEN3_INCLUDE_DIR)
LDFLAGS  =
MY_LIBS  =
PROGRAM  = similarity-3d
RUNNER   = run-tests
SRCDIRS  = src src/*
TSTDIRS  = tests
SRCEXTS  = .c .cc .cpp .cxx
HDREXTS  = .h .hpp .h++ .hxx
CTAGS    = ctags

PREFIX  ?=/usr/local/bin/
DEBUG   ?= 1
RELEASE ?= 0
TESTARGS ?= --order lex

EMPTY =
SPACE = $(EMPTY) $(EMPTY)

ifeq ($(PROGRAM),)
  CUR_PATH_NAMES = $(subst /,$(SPACE),$(subst $(SPACE),_,$(CURDIR)))
  PROGRAM = $(word $(words $(CUR_PATH_NAMES)),$(CUR_PATH_NAMES))
  ifeq ($(PROGRAM),)
    PROGRAM = a.out
  endif
endif

ifeq ($(SRCDIRS),)
  SRCDIRS = .
endif

SOURCES = $(foreach d,$(SRCDIRS), $(wildcard $(addprefix $(d)/*,$(SRCEXTS))))
HEADERS = $(foreach d,$(SRCDIRS), $(wildcard $(addprefix $(d)/*,$(HDREXTS))))
SRC_CXX = $(filter-out %.c, $(SOURCES))
OBJECTS = $(addsuffix .o, $(basename $(SOURCES)))

TESTSRC = $(foreach d,$(TSTDIRS), $(wildcard $(addprefix $(d)/*,$(SRCEXTS))))
TESTHDR = $(foreach d,$(TSTDIRS), $(wildcard $(addprefix $(d)/*,$(HDREXTS))))
TST_CXX = $(filter-out %.c, $(SOURCES))

MAIN    = $(foreach d,$(SRCDIRS), $(addprefix $(d)/,main.o))
TSTMAIN = $(foreach d,$(TSTDIRS), $(addprefix $(d)/,$(RUNNER).cpp))

TSTOBJS = $(addsuffix .o, $(basename $(TSTMAIN)))
TSTOBJS += $(filter-out $(MAIN), $(OBJECTS))
TSTOBJS += $(filter-out $(TSTMAIN), $(TESTSRC))

# Sets whether project is in debug or release mode
ifeq ($(RELEASE), 1)
  CFLAGS   += -O3 -s
  CXXFLAGS += -O3 -s
  DEBUG = 0
endif

ifeq ($(DEBUG), 1)
  CFLAGS   += -DDEBUG -ggdb3
  CXXFLAGS += -DDEBUG -ggdb3
endif

COMPILE.c   = $(CC) $(EXTFLAGS) $(CFLAGS) -c
COMPILE.cxx = $(CXX) $(EXTFLAGS) $(CXXFLAGS) -c
LINK.c      = $(CC) $(EXTFLAGS) $(CFLAGS) $(LDFLAGS)
LINK.cxx    = $(CXX) $(EXTFLAGS) $(CXXFLAGS) $(LDFLAGS)

.PHONY: all objs tests tags clean help install uninstall
all: $(PROGRAM)

tests: $(RUNNER)
	./$(RUNNER) $(TESTARGS)

install: all
	install -m 0700 $(PROGRAM) $(PREFIX)$(PROGRAM)

uninstall: all
	rm -f $(PREFIX)$(PROGRAM)

# Rules for building object files

%.o:%.c
	$(COMPILE.c) $< -o $@

%.o:%.cc
	$(COMPILE.cxx) $< -o $@

%.o:%.cpp
	$(COMPILE.cxx) $< -o $@

%.o:%.cxx
	$(COMPILE.cxx) $< -o $@

# Rules for generating executable
$(PROGRAM): $(OBJECTS)
ifeq ($(SRC_CXX),)              # C program
	$(LINK.c)   $(OBJECTS) $(MY_LIBS) -o $@
	@echo Type ./$@ to execute the program.
else                            # C++ program
	$(LINK.cxx) $(OBJECTS) $(MY_LIBS) -o $@
	@echo Type ./$@ to execute the program.
endif

$(RUNNER): $(TSTOBJS)
ifeq ($(TST_CXX),) # There are no CXX/CPP files
	$(LINK.c)   $(TSTOBJS) $(MY_LIBS) -o $@
else # C++ program
	$(LINK.cxx) $(TSTOBJS) $(MY_LIBS) -o $@
endif

tags: $(HEADERS) $(SOURCES)
	$(CTAGS) -R --sort=yes --c++-kinds=+pl --fields=+iaS --extra=+q $(HEADERS) $(SOURCES)

clean:
	rm $(OBJECTS) $(foreach d,$(TSTDIRS), $(d)/*.o) $(PROGRAM)

help:
	@echo 'make [all]     :  builds project (note: debug is enabled by default)'
	@echo 'make tests     :  builds tests for project.'
	@echo 'make DEBUG=1   :  builds project with debug info (-DDEBUG and -ggdb3)'
	@echo 'make RELEASE=1 :  builds project with optimizations (-O3 and -s)'
	@echo 'make install   :  builds project and installs to PREFIX/PROJECTNAME (default /usr/local/bin/PROJECTNAME)'
	@echo 'make uninstall :  removes installed project executable at PREFIX/PROJECTNAME'
	@echo 'make tags      :  builds a ctags file for the project'
	@echo 'make clean     :  removes object files and binaries, cleans directory'
	@echo 'make help      :  this text.'
