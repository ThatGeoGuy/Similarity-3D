# Similarity 3D

A small C++ program to estimate 3D similarity transformation parameters and
transform points into the desired coordinate system.

## Build Instructions

The `similarity-3d` program is built using a standard Makefile. There is only
one real dependency, which is on
the [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page) library.
Pointing the Makefile to the correct installation of Eigen3 is as simple as
modifying the `EIGEN3_INCLUDE_DIR` environment variable. The tests
use [Catch](http://catch-lib.net), however, the corresponding header for Catch
is kept in the `tests/` directory, so there is no need to worry about which
version to download or use. Building is as simple as follows.

```
$ git clone https://bitbucket.org/ThatGeoGuy/similarity-3d.git
$ cd similarity-3d/
$ make -j 4 all RELEASE=1 # For release mode.
```

To run the test suite, you can make the tests, which will likewise call the test
runner:

```
$ make tests
```

Do note that you do not want to build the tests in parallel mode (i.e. with
arguments `-j 4`, it won't help), and you can modify the runner arguments (i.e.
which tests are run) at run-time by changing the `TESTARGS` environment
variable.

```
$ make tests TESTARGS=[similarity]
```

## Usage

Using the `similarity-3d` program is quite simple. To get started, you can run
`similarity-3d` with the help flag (`-h`):

```
$ similarity-3d -h
Usage: similarity-3d [-hprn] [-t POINTS-FILE] MODEL-FILE
        -h      Print help text.
        -p      Print estimated parameters.
        -r      Output adjustment residuals.
        -n      Only use approximates (Horn's method).
        -t      Transform points by the estimated parameters.
```

Note that the one required argument, the model file, is a white-space delimited
text file with the following columns:

```
<POINT LABEL> <X1> <Y1> <Z1> <X2> <Y2> <Z2>
```

The points file is just a white-space delimited file with `<X> <Y> <Z>` to be
transformed from coordinate system 1 to coordinate system 2 in the model file.
If you want to read the points in from `stdin`, you can call the program as
follows:

```
$ similarity-3d -t - model-file.txt
```

## LICENSE

Project is distributed under the terms of the GPLv3+. See `LICENSE` for more
details.
