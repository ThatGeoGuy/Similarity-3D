// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Defines a template for defining custom exceptions
 */

#pragma once

#include <exception>
#include <string>

/*
  TException is a template class used to define a message exception
  type. It takes a constant reference to a std::string to define an
  instance of the template, which will serve as the name of the template.

  e.g. if you want an "ArgumentException", you must pass what is
  effectively TException<"Argument">, however, there is one caveat:
  Since templates can only specialize on integral types, you need to
  pass this as a reference.

  std::string arg("Argument");
  using ArgumentException = TException<arg>;

  In order to prevent arg from being modified (since it can't be const),
  we instead suggest that you hide it from other users using the
  anonymous namespace.

  namespace { std::string arg("Argument"); }
  using ArgumentException = TException<arg>;

  @tparam T - Reference to a string describing the exception name.

  @param msg - Message that can be any char* or string describing the
               exception.
*/
template<const std::string& T>
class TException : virtual public std::exception
{
public:
    TException(std::basic_string<char> msg) noexcept
    : _message(std::move(msg))
    { }

    /*
      Describes the exception as a null terminated character sequence.

      @returns null terminated char sequence describing the exception
    */
    const char* what() const noexcept override
    {
        return (T + " Exception: " + _message).c_str();
    }

private:
    std::string _message;
};
