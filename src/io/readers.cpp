// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "readers.h"

#include <string>

void read_matched_points(
    std::ifstream& input,
    Dictionary<const Point3D>& from,
    Dictionary<const Point3D>& to)
{
    if (!input.good()) {
        throw ArgumentException(
            "Input file cannot be read -- it's no good.");
    }

    double X1, Y1, Z1;
    double X2, Y2, Z2;
    std::string label;

    while (input >> label >> X1 >> Y1 >> Z1 >> X2 >> Y2 >> Z2) {
        from.insert(DictEntry<Point3D>(Label(label), Point3D{X1, Y1, Z1}));
        to.insert(DictEntry<Point3D>(Label(label), Point3D{X2, Y2, Z2}));
    }
}
