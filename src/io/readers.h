// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

#include "../exceptions/ArgumentException.h"
#include "../misc/Dictionary.hpp"
#include "../points/Point3D.h"

/*
  Reads in a .inp file, which is a file that contains two sets of matched 3D
  points in the following format:

    <LABEL> <X1> <Y1> <Z1> <X2> <Y2> <Z2>

  These points are inserted into the two dictionary arguments respectively. The
  two sets of XYZ values should represent matched points (with the same label)
  in two coordinate systems. These are usually denoted as the left and right
  system, or as the "from" and "to" system in similarity-3d.

  Throws an ArgumentException if the input stream is not good.

  @param input - the input stream to read from.
  @param from  - the left side coordinate system X1, Y1, Z1
  @param to    - the right side coordiante system X2, Y2, Z2

  @returns destructively modifies the input arguments and returns the read in
           points from the file.
*/
void read_matched_points(
    std::ifstream&, Dictionary<const Point3D>&, Dictionary<const Point3D>&);
