// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "usage.h"

void print_usage(std::ostream& sout, const std::string& program_name) noexcept
{
    using std::endl;

    sout << "Usage: " << program_name
         << " [-hprn] [-t POINTS-FILE] MODEL-FILE" << endl
         << "\t" << "-h\t" << "Print help text." << endl
         << "\t" << "-p\t" << "Print estimated parameters." << endl
         << "\t" << "-r\t" << "Output adjustment residuals." << endl
         << "\t" << "-n\t" << "Only use approximates (Horn's method)." << endl
         << "\t" << "-t\t"
         << "Transform points by the estimated parameters." << endl;
}
