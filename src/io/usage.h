// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Defines a method to print the help text for the program.

#pragma once

#include <iostream>
#include <string>

/* Prints the usage information for the program.
 *
 * @param program_name - the name of the program once compiled. Usually taken as
 *                       argv[0]
 */
void print_usage(std::ostream&, const std::string&) noexcept;
