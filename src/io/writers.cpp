// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "writers.h"

#include "../misc/angles.h"

void write_parameters(
    std::ostream& sout,
    const SimilarityTransformationParameters& parameters,
    int width,
    int precision)
{
    auto pdict = parameters.to_dict();
    pdict[Label("Omega [deg]")] = rad_to_deg(pdict[Label("Omega [deg]")]);
    pdict[Label("Phi [deg]")] = rad_to_deg(pdict[Label("Phi [deg]")]);
    pdict[Label("Kappa [deg]")] = rad_to_deg(pdict[Label("Kappa [deg]")]);

    sout << "Parameters:" << std::endl
         << std::setw(width)
         << std::setprecision(precision)
         << pdict << std::endl;
}
