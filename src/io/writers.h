// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

#include "../misc/Dictionary.hpp"
#include "../transforms/SimilarityTransformationParameters.h"

/*
  Writes the provided parameter set to the given ostream.

  The precision and width arguments are used to set the precision and width
  of the output stream.

  @param sout       - the stream to print out to.
  @param parameters - the similarity transformation parameters to print.
  @param width      - the width to print the dictionary at.
  @param precision  - the precision to print the parameter values to.
*/
void write_parameters(
    std::ostream&, const SimilarityTransformationParameters&, int, int);
