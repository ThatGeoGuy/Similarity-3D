// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>

#include <getopt.h>
#include <iostream>
#include <iomanip>
#include <string>

#include "exceptions/ArgumentException.h"
#include "exceptions/TransformationException.h"
#include "io/usage.h"
#include "io/readers.h"
#include "io/writers.h"
#include "misc/angles.h"
#include "misc/Dictionary.hpp"
#include "points/Point3D.h"
#include "transforms/HornsRegistrationEstimator.h"
#include "transforms/SimilarityTransformEstimator.h"
#include "transforms/transform_points.h"

constexpr int WIDTH = 18;
constexpr int PRECISION = 4;

int main(int argc, char** argv)
{
    // Remove sync with c-style IO since I'm not using it anywhere.
    std::cout.sync_with_stdio(false);
    std::cerr.sync_with_stdio(false);
    std::cin.sync_with_stdio(false);

    // Ensure that output is fixed precision
    std::cout.setf(std::ios_base::fixed);
    std::cerr.setf(std::ios_base::fixed);

    auto print_parameters = false;
    auto print_residuals = false;
    auto only_use_approximates = false;

    auto points_filename = std::string{""};

    int opt;
    while ((opt = getopt(argc, argv, "hprnt:")) != -1) {
        switch (opt) {
        case 'p':
            print_parameters = true;
            break;
        case 'r':
            print_residuals = true;
            break;
        case 'n':
            only_use_approximates = true;
            break;
        case 't':
            points_filename = std::string{optarg};
            break;
        case 'h':
            print_usage(std::cout, argv[0]);
            return EXIT_SUCCESS;
        default:
            print_usage(std::cerr, argv[0]);
            return EXIT_FAILURE;
        }
    }

    if (optind + 1 > argc) {
        std::cerr << "Missing required argument. See usage." << std::endl;
        print_usage(std::cerr, argv[0]);
        return EXIT_FAILURE;
    }

    auto model_file = std::string{argv[optind]};
    std::ifstream model_stream;
    model_stream.open(model_file, std::ifstream::in);

    auto from_points = Dictionary<const Point3D>{};
    auto to_points = Dictionary<const Point3D>{};

    try {
        read_matched_points(model_stream, from_points, to_points);
        model_stream.close();
    } catch (ArgumentException& exn) {
        std::cerr << exn.what() << std::endl;
        return EXIT_FAILURE;
    }

    std::unique_ptr<ITransformEstimator<Point3D,
                                        SimilarityTransformationParameters>>
        estimator = nullptr;

    try {
        if (only_use_approximates) {
            estimator = std::make_unique<HornsRegistrationEstimator>(
                from_points,
                to_points
            );
        } else {
            estimator = std::make_unique<SimilarityTransformEstimator>(
                from_points,
                to_points
            );
        }
    } catch (ArgumentException& exn) {
        std::cerr << exn.what() << std::endl;
        return EXIT_FAILURE;
    }

    auto parameters = SimilarityTransformationParameters{};
    try {
        parameters = estimator->parameters();
    } catch (TransformationException& exn) {
        std::cerr << exn.what() << std::endl;
        return EXIT_FAILURE;
    }

    if (print_parameters) {
        try {
            write_parameters(
                std::cout, parameters, WIDTH, PRECISION+4);
        } catch (TransformationException& exn) {
            std::cerr << exn.what() << std::endl;
            return EXIT_FAILURE;
        }
    }

    if (print_residuals) {
        std::cout << "Residuals: (point | vx | vy | vz)" << std::endl
                  << std::setw(WIDTH-9) // -9 because these are small!
                  << std::setprecision(PRECISION)
                  << estimator->residuals() << std::endl;
    }

    // Quick exit if no points are to be transformed.
    if (points_filename == "") {return EXIT_SUCCESS;}

    if (points_filename == "-") {
        transform_points(std::cin, parameters, 0, PRECISION);
    } else {
        std::ifstream points_stream;
        points_stream = std::ifstream(points_filename, std::ifstream::in);
        transform_points(points_stream, parameters, 0, PRECISION);
    }

    return EXIT_SUCCESS;
}
