// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Header providing a type for representing a dictionary of points
*/

#pragma once

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <map>
#include <vector>
#include <string>

#include "Labels.h"

/*
  Primary Dictionary type. Should correspond to a map that contains
  `label` -> `object`
*/
template<typename T>
using Dictionary = std::map<Label, T>;

/*
  Specifies the type of entry into the Dictionary object.
  AKA what to pass into Dict.insert();
*/
template<typename T>
using DictEntry = std::pair<Label, T>;

/*
  Predicate which determines whether one Dictionary is a subset of another.
 */
template<typename T1, typename T2>
bool is_subset(const Dictionary<T1>& pd1, const Dictionary<T2>& pd2) noexcept {
    for (auto pair : pd1) {
        if (pd2.count(pair.first)) continue;
        return false;
    }
    return true;
}

/*
  Returns a list of all the labels in a dictionary
 */
template <typename T>
std::vector<Label> labels(const Dictionary<T>& dict)
{
    auto ret = std::vector<Label>{};
    ret.reserve(dict.size());

    std::transform(std::begin(dict), std::end(dict), std::back_inserter(ret),
                   [](auto pair) { return pair.first; });
    return ret;
}

/*
  Operator overload to output a dictionary to an ostream
 */
template <typename T>
std::ostream& operator<<(std::ostream& out, const Dictionary<T>& dict)
{
    auto width = out.width();
    std::for_each(std::begin(dict), std::end(dict),
                  [&out, &width](auto pair) {
                      out << std::setw(width) << pair.first << "  "
                          << std::setw(width) << pair.second << std::endl;
                  });
    return out;
}
