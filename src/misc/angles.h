// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Implements basic conversion procedures for degrees and radians
*/

#pragma once

/*
  Converts from degrees to radians

  @param angle - the angle in degrees.

  @returns the angle in radians
*/
double deg_to_rad(double angle) noexcept;

/*
  Converts from radians to degrees

  @param angle - the angle in radians

  @returns the angle in degrees
*/
double rad_to_deg(double angle) noexcept;
