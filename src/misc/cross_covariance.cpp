// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "cross_covariance.h"

#include "../points/centroid.h"

Eigen::Matrix<double, 3, 3> cross_covariance(
    const Dictionary<const Point3D>& left,
    const Dictionary<const Point3D>& right)
{
    auto xcov = Eigen::Matrix<double, 3, 3>(3, 3);

    auto ps = reduce_to_centroid(left);
    auto qs = reduce_to_centroid(right);
    auto names = labels(ps);

    for (auto name : names) {
        xcov(0, 0) += ps[name].X * qs[name].X;
        xcov(0, 1) += ps[name].X * qs[name].Y;
        xcov(0, 2) += ps[name].X * qs[name].Z;

        xcov(1, 0) += ps[name].Y * qs[name].X;
        xcov(1, 1) += ps[name].Y * qs[name].Y;
        xcov(1, 2) += ps[name].Y * qs[name].Z;

        xcov(2, 0) += ps[name].Z * qs[name].X;
        xcov(2, 1) += ps[name].Z * qs[name].Y;
        xcov(2, 2) += ps[name].Z * qs[name].Z;
    }
    return xcov;
}
