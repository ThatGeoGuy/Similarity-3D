// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Eigen/Dense>

#include "Dictionary.hpp"
#include "../points/Point3D.h"

/*
  Computes the cross covariance between two sets of points.

  @param left  - The first set of points.
  @param right - The second set of points.

  @returns a 3x3 matrix denoting the cross covariance between the data.
*/
Eigen::Matrix<double, 3, 3> cross_covariance(const Dictionary<const Point3D>&,
                                             const Dictionary<const Point3D>&);
