// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Implements Point3D.h
 */


#include "Point3D.h"

#include <cmath>
#include <iomanip>

std::ostream& operator<<(std::ostream& output, const Point3D& pt)
{
    auto width = output.width();
    return (output << std::setw(width) << pt.X << "  "
                   << std::setw(width) << pt.Y << "  "
                   << std::setw(width) << pt.Z);
}

double distance(const Point3D& p, const Point3D& q)
{
    return sqrt(pow(q.X - p.X, 2) +
                pow(q.Y - p.Y, 2) +
                pow(q.Z - p.Z, 2));
}
