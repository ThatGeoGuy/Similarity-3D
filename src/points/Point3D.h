// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Defines a 3D point type
*/

#pragma once

#include <fstream>

struct Point3D
{
    double X;
    double Y;
    double Z;
};

std::ostream& operator<<(std::ostream&, const Point3D&);

/*
  Computes the distance between two 3D points.

  @param p - the first point
  @param q - the second points

  @returns the euclidean distance between the points.
*/
double distance(const Point3D&, const Point3D&);
