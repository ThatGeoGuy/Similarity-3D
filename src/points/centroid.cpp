// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "centroid.h"

#include <algorithm>
#include <iterator>
#include <numeric>

using std::begin;
using std::end;
using std::back_inserter;

template<typename T>
struct MeanAccumulator {
    T mean;
    size_t length;
};

Point3D centroid(const Dictionary<const Point3D>& points)
{
    using std::accumulate;
    auto ret = accumulate(begin(points),
                          end(points),
                          MeanAccumulator<Point3D>{Point3D{}, 0},
                          [](auto acc, auto pair) {
                              auto pt = pair.second;
                              auto nlength = acc.length + 1;
                              auto gain = 1 / nlength;
                              auto q = Point3D{
                                  acc.mean.X + gain * (pt.X - acc.mean.X),
                                  acc.mean.Y + gain * (pt.Y - acc.mean.Y),
                                  acc.mean.Z + gain * (pt.Z - acc.mean.Z)
                              };
                              return MeanAccumulator<Point3D>{q, nlength};
                          });
    return ret.mean;
}

Dictionary<const Point3D>
reduce_to_centroid(const Dictionary<const Point3D>& points)
{
    auto ret = Dictionary<const Point3D>{};
    auto c = centroid(points);
    for (const auto& pair : points) {
        auto name = pair.first;
        auto pt = pair.second;
        ret.insert(DictEntry<const Point3D>(
                       name,
                       Point3D{pt.X - c.X,
                               pt.Y - c.Y,
                               pt.Z - c.Z}));
    }
    return ret;
}
