// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "../misc/Dictionary.hpp"
#include "Point3D.h"

/*
  Computes the centroid of a group of labeled points.

  @param points - the points to find the centroid of.

  @returns a point representing the centroid of a group of points.
*/
Point3D centroid(const Dictionary<const Point3D>&);

/*
  Reduces the input points to their centroid (translation to centroid).

  @param points - the points to reduce to the centroid.

  @returns a collection of points reduced to their common centroid.
*/
Dictionary<const Point3D> reduce_to_centroid(const Dictionary<const Point3D>&);
