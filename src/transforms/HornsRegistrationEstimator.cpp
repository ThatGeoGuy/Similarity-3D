// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "HornsRegistrationEstimator.h"

#include "../misc/cross_covariance.h"
#include "../points/centroid.h"
#include "rotation.h"
#include "SimilarityTransform.h"


struct HornsRegistrationEstimator::Impl {
    Dictionary<const Point3D> from_points;
    Dictionary<const Point3D> to_points;
    SimilarityTransformationParameters parameters;
    bool is_computed;

    /*
      Default constructor for implementation struct.
    */
    explicit Impl();
    /*
      Constructs a new implementation object for the similarity transform
      estimator, initialized with points from the to and from coordinate frames.
    */
    explicit Impl(const Dictionary<const Point3D>& from,
                  const Dictionary<const Point3D>& to);
    /*
      Implementations for the corresponding interface methods.
    */
    void add(const Label& label, Point3D from, Point3D to) noexcept;

    void remove(const Label& label) noexcept;

    std::vector<Label> point_names() const noexcept;

    SimilarityTransformationParameters get_parameters();

    /*
      Estimates the similarity transformation parameters from the current state
      of the estimator object. Throws a TransformationException if there are not
      enough points to estimate the transformation parameters or if the
      parameters cannot be computed due to a rank deficiency.
    */
    void estimate_parameters();

    /*
      Produces and returns a set of residuals for the transformation error
      between the to and from systems.
    */
    Dictionary<const Point3D> residuals();
};

HornsRegistrationEstimator::Impl::Impl()
    : from_points(Dictionary<const Point3D>{}),
      to_points(Dictionary<const Point3D>{}),
      parameters(SimilarityTransformationParameters{}),
      is_computed(false)
{ }

HornsRegistrationEstimator::Impl::Impl(
    const Dictionary<const Point3D>& from,
    const Dictionary<const Point3D>& to)
    : from_points(std::move(from)),
      to_points(std::move(to)),
      parameters(SimilarityTransformationParameters{}),
      is_computed(false)
{
    if (!is_subset(from_points, to_points) ||
        !is_subset(to_points, from_points)) {
        throw ArgumentException(
            "Both sets of labeled points must be matched in the estimator.");
    }
}

void HornsRegistrationEstimator::Impl::add(const Label& label, Point3D from, Point3D to) noexcept
{
    // Remove the point in case it already exists
    remove(label);

    from_points.insert(DictEntry<const Point3D>(label, std::move(from)));
    to_points.insert(DictEntry<const Point3D>(label, std::move(to)));
    is_computed = false;
}

void HornsRegistrationEstimator::Impl::remove(const Label& label) noexcept
{
    if (from_points.count(label)) {
        from_points.erase(label);
    }
    if (to_points.count(label)) {
        to_points.erase(label);
    }
    is_computed = false;
}

std::vector<Label> HornsRegistrationEstimator::Impl::point_names() const noexcept
{
    return labels(from_points);
}

SimilarityTransformationParameters
HornsRegistrationEstimator::Impl::get_parameters()
{
    estimate_parameters();
    return parameters;
}

static inline Eigen::Vector4d horns_rotation(
    const Dictionary<const Point3D>& left,
    const Dictionary<const Point3D>& right)
{
    auto s = cross_covariance(right, left);

    auto N = Eigen::Matrix<double, 4, 4>(4, 4);
    N <<
        (s(0, 0) + s(1, 1) + s(2, 2)),
        (s(1, 2) - s(2, 1)),
        (s(2, 1) - s(1, 2)),
        (s(0, 1) - s(1, 0)),

        (s(1, 2) - s(2, 1)),
        (s(0, 0) - s(1, 1) - s(2, 2)),
        (s(0, 1) + s(1, 0)),
        (s(0, 2) + s(2, 0)),

        (s(2, 0) - s(0, 2)),
        (s(0, 1) + s(1, 0)),
        (s(1, 1) - s(0, 0) - s(2, 2)),
        (s(1, 2) + s(2, 1)),

        (s(0, 1) - s(1, 0)),
        (s(2, 0) + s(0, 2)),
        (s(1, 2) + s(2, 1)),
        (s(2, 2) - s(1, 1) - s(0, 0));

    auto eig = Eigen::EigenSolver<Eigen::Matrix<double, 4, 4>>{N};
    const auto evals = eig.eigenvalues().real();
    const auto evecs = eig.eigenvectors().real();

    Eigen::VectorXd::Index max_index;
    evals.maxCoeff(&max_index);

    return evecs.col(max_index);
}

static inline double horns_scale(
    const Dictionary<const Point3D>& left,
    const Dictionary<const Point3D>& right)
{
    auto names = labels(left);

    double mean_scale = 0;
    size_t nelem = 0;

    for (auto i = 0U; i < names.size(); ++i) {
        for (auto j = i + 1; j < names.size(); ++j) {
            auto distl = distance(left.find(names.at(i))->second,
                                  left.find(names.at(j))->second);
            auto distr = distance(right.find(names.at(i))->second,
                                  right.find(names.at(j))->second);

            ++nelem;
            mean_scale += (1 / (nelem)) * ((distr / distl) - mean_scale);
        }
    }

    return mean_scale;
}

void HornsRegistrationEstimator::Impl::estimate_parameters()
{
    if (is_computed) { return; }

    if (from_points.size() < 3 || to_points.size() < 3) {
        throw TransformationException(
            "Insufficient number of points to perform "
            "Horn's parameter estimation.");
    }

    const auto q = horns_rotation(from_points, to_points);
    const auto rot = EulerAngles::from_unit_quaternion(q(0), q(1), q(2), q(3));
    const auto M = rotation_matrix(q(0), q(1), q(2), q(3));

    const auto left_centroid = centroid(from_points);
    const auto right_centroid = centroid(to_points);

    const auto scale = horns_scale(from_points, to_points);

    auto rl = Eigen::Vector3d{left_centroid.X,
                              left_centroid.Y,
                              left_centroid.Z};
    auto rr = Eigen::Vector3d{right_centroid.X,
                              right_centroid.Y,
                              right_centroid.Z};

    auto trn = rr - (scale * (M * rl));

    parameters = SimilarityTransformationParameters{
        trn(0), trn(1), trn(2), // X, Y, Z
        rot.omega, rot.phi, rot.kappa,
        scale
    };
    is_computed = true;
}

Dictionary<const Point3D> HornsRegistrationEstimator::Impl::residuals()
{
    estimate_parameters();
    const auto st = SimilarityTransform(parameters);
    const auto names = point_names();

    auto ret = Dictionary<const Point3D>{};
    for (const auto& name : names) {
        const auto from = from_points.at(name);
        const auto to = to_points.at(name);

        const auto tpt = st.transform(from);
        ret.insert(
            DictEntry<const Point3D>(
                name,
                Point3D{tpt.X - to.X, tpt.Y - to.Y, tpt.Z - to.Z}));
    }

    return ret;
}


/*
  Public interface(s) for the HornsRegistrationEstimator class.
*/

HornsRegistrationEstimator::HornsRegistrationEstimator()
    : _pImpl(std::make_unique<HornsRegistrationEstimator::Impl>())
{ }

HornsRegistrationEstimator::HornsRegistrationEstimator(
    const Dictionary<const Point3D>& from_points,
    const Dictionary<const Point3D>& to_points)
    : _pImpl(std::make_unique<HornsRegistrationEstimator::Impl>(from_points, to_points))
{ }

HornsRegistrationEstimator::HornsRegistrationEstimator(
    HornsRegistrationEstimator&&) = default;

HornsRegistrationEstimator&
HornsRegistrationEstimator::operator=(HornsRegistrationEstimator&&) = default;

HornsRegistrationEstimator::~HornsRegistrationEstimator() = default;

void HornsRegistrationEstimator::add(
    const Label& label, Point3D from, Point3D to) noexcept
{
    _pImpl->add(label, std::move(from), std::move(to));
}

void HornsRegistrationEstimator::remove(const Label& label) noexcept
{
    _pImpl->remove(label);
}

std::vector<Label>
HornsRegistrationEstimator::point_names() const noexcept
{
    return _pImpl->point_names();
}

SimilarityTransformationParameters
HornsRegistrationEstimator::parameters()
{
    return _pImpl->get_parameters();
}

Dictionary<const Point3D> HornsRegistrationEstimator::residuals()
{
    return _pImpl->residuals();
}
