// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>
#include <vector>

#include "../exceptions/ArgumentException.h"
#include "../exceptions/TransformationException.h"
#include "ITransformEstimator.h"
#include "../misc/Dictionary.hpp"
#include "../misc/Labels.h"
#include "../points/Point3D.h"
#include "SimilarityTransformationParameters.h"

class HornsRegistrationEstimator
: public ITransformEstimator<Point3D, SimilarityTransformationParameters> {
public:
    HornsRegistrationEstimator();

    HornsRegistrationEstimator(const Dictionary<const Point3D>&,
                               const Dictionary<const Point3D>&);

    HornsRegistrationEstimator(HornsRegistrationEstimator&) = delete;
    HornsRegistrationEstimator& operator=(HornsRegistrationEstimator&) = delete;

    HornsRegistrationEstimator(HornsRegistrationEstimator&&);
    HornsRegistrationEstimator& operator=(HornsRegistrationEstimator&&);

    ~HornsRegistrationEstimator();

    /*
      Adds a point from the corresponding from and to coordinate frames to
      the estimator. If a point with a given label has already been added and
      is added again, the new point values overwrite the old point values.

      @param label - the point's name / label / identifier
      @param from  - the point in the coordinate system you are transforming
                     from.
      @param to    - the point in the coordinate system you are transforming
                     to.
    */
    void add(const Label&, Point3D, Point3D) noexcept override;

    /*
      Removes a point with a given label from the estimator. If the point has
      not been added the estimator prior to this call, this does nothing.

      @param label - the name / label / identifier of the point to remove.
    */
    void remove(const Label&) noexcept override;

    /*
      Returns a vector containing all the names of the points added to the
      estimator.
    */
    std::vector<Label> point_names() const noexcept override;

    /*
      Returns the set of SimilarityTransformationParameters estimated by the
      estimator. This comprises a set of 7 parameters (3 translations, 3
      rotation, and 1 scale) that define a 3D coordinate transformation between
      two coordinate frames.
    */
    SimilarityTransformationParameters parameters() override;

    /*
      Returns the set of 3D residuals (stored as 3D points) for each point that
      was used in the adjustment to compute the similarity transformation
      parameters.
    */
    Dictionary<const Point3D> residuals() override;

private:
    struct Impl;
    std::unique_ptr<Impl> _pImpl;
};
