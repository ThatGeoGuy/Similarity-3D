// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Header providing a base interface class for coordinate transformations
*/

#pragma once

#include <vector>

#include <Eigen/Dense>

#include "../misc/Dictionary.hpp"
#include "../misc/Labels.h"

template <typename PointType, typename ParameterType>
class ITransformEstimator
{
public:
    virtual ~ITransformEstimator() {};

    virtual void add(const Label&, PointType, PointType) noexcept = 0;
    virtual void remove(const Label&) noexcept = 0;
    virtual std::vector<Label> point_names() const noexcept = 0;
    virtual ParameterType parameters() = 0;
    virtual Dictionary<const PointType> residuals() = 0;
};
