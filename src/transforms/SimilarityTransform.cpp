// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "SimilarityTransform.h"

#include "rotation.h"

struct SimilarityTransform::Impl {
    const SimilarityTransformationParameters parameters;

    Impl(const SimilarityTransformationParameters&);

    /*
      Getter function for the stored parameters.
    */
    SimilarityTransformationParameters get_parameters() const noexcept;

    /*
      Implementation of the public transform method.
    */
    Point3D transform(const Point3D&) const noexcept;

    /*
      Implementation of the public transform_all method.
    */
    Eigen::MatrixX3d transform_all(const Eigen::MatrixX3d&) const noexcept;
};

SimilarityTransform::Impl::Impl(const SimilarityTransformationParameters& _parameters)
    : parameters(_parameters)
{ }

SimilarityTransformationParameters
SimilarityTransform::Impl::get_parameters() const noexcept
{
    return parameters;
}

Point3D
SimilarityTransform::Impl::transform(const Point3D& point) const noexcept
{
    const auto r = Eigen::Vector3d{point.X, point.Y, point.Z};
    const auto t = Eigen::Vector3d{parameters.Tx, parameters.Ty, parameters.Tz};
    const auto M = rotation_matrix(parameters.W, parameters.P, parameters.K);
    const auto scale = parameters.scale;

    const auto rp = (scale * (M * r)) + t;
    return Point3D{rp(0), rp(1), rp(2)};
}

Eigen::MatrixX3d
SimilarityTransform::Impl::transform_all(const Eigen::MatrixX3d& points)
    const noexcept
{
    const auto rs = points.transpose();
    const auto t = Eigen::Vector3d{parameters.Tx, parameters.Ty, parameters.Tz};
    const auto M = rotation_matrix(parameters.W, parameters.P, parameters.K);
    const auto scale = parameters.scale;

    auto rps = (scale * (M * rs)).colwise() + t;
    return rps.transpose();
}

/*
  Public interface definitions
*/

SimilarityTransform::SimilarityTransform(
    const SimilarityTransformationParameters& parameters)
    : _pImpl(std::make_unique<SimilarityTransform::Impl>(parameters))
{ }

SimilarityTransform::SimilarityTransform(SimilarityTransform&&) = default;
SimilarityTransform& SimilarityTransform::operator=(SimilarityTransform&&) = default;

SimilarityTransform::~SimilarityTransform() = default;


SimilarityTransformationParameters
SimilarityTransform::parameters() const noexcept
{
    return _pImpl->get_parameters();
}

Point3D SimilarityTransform::transform(const Point3D& point) const noexcept
{
    return _pImpl->transform(point);
}

Eigen::MatrixX3d
SimilarityTransform::transform_all(const Eigen::MatrixX3d& points) const noexcept
{
    return _pImpl->transform_all(points);
}
