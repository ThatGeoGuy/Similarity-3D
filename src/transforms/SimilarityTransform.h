// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <memory>

#include <Eigen/Dense>

#include "../points/Point3D.h"
#include "SimilarityTransformationParameters.h"

class SimilarityTransform {
public:
    SimilarityTransform(const SimilarityTransformationParameters&);

    SimilarityTransform(SimilarityTransform&) = delete;
    SimilarityTransform& operator=(SimilarityTransform&) = delete;

    SimilarityTransform(SimilarityTransform&&);
    SimilarityTransform& operator=(SimilarityTransform&&);

    ~SimilarityTransform();

    /*
      Returns the parameters that define the transform.
    */
    SimilarityTransformationParameters parameters() const noexcept;

    /*
      Transforms a single point object using the transformation parameters.

      @param point - the point to transform.

      @returns a point in the new coordinate system transformed by the
               given transformation parameters.
    */
    Point3D transform(const Point3D&) const noexcept;

    /*
      Transforms a collection of points, stored as an N x 3 matrix using the
      transformation parameters.

      @param points - an N x 3 matrix (columns X, Y, Z) that defines a
                      collection of points to transform.

      @returns an N x 3 matrix holding all the points transformed into the new
               coordinate frame.
    */
    Eigen::MatrixX3d transform_all(const Eigen::MatrixX3d&) const noexcept;

private:
    struct Impl;
    std::unique_ptr<Impl> _pImpl;
};
