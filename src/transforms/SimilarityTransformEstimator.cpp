// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "SimilarityTransformEstimator.h"

#include "HornsRegistrationEstimator.h"
#include "partial_derivatives.h"
#include "rotation.h"
#include "SimilarityTransform.h"


struct SimilarityTransformEstimator::Impl {
    Dictionary<const Point3D> from_points;
    Dictionary<const Point3D> to_points;
    SimilarityTransformationParameters parameters;
    Eigen::Matrix<double, 7, 7> parameter_variances;
    bool is_computed;

    /*
      Default constructor for implementation struct.
    */
    explicit Impl();
    /*
      Constructs a new implementation object for the similarity transform
      estimator, initialized with points from the to and from coordinate frames.
    */
    explicit Impl(const Dictionary<const Point3D>& from,
                  const Dictionary<const Point3D>& to);
    /*
      Implementations for the corresponding interface methods.
    */
    void add(const Label& label, Point3D from, Point3D to) noexcept;

    void remove(const Label& label) noexcept;

    std::vector<Label> point_names() const noexcept;

    SimilarityTransformationParameters get_parameters();

    /*
      Helper method to acquire an initial approximation of the 3D similarity
      transformation.
    */
    SimilarityTransformationParameters initial_parameter_approximations() const;

    /*
      Helper method to construct the first design (model jacobian) matrix for
      the least squares estimator.

      @param parameters - the approximate similarity transformation parameters.
    */
    Eigen::MatrixXd first_design(const SimilarityTransformationParameters&) const;

    /*
      Helper method to construct the misclosure vector for the least squares
      estimator.

      @param parameters - the approximate similarity transformation parameters.
    */
    Eigen::VectorXd misclosure(const SimilarityTransformationParameters&) const;

    /*
      Estimates the similarity transformation parameters from the current state
      of the estimator object. Throws a TransformationException if there are not
      enough points to estimate the transformation parameters or if the
      parameters cannot be computed due to a rank deficiency.
    */
    void estimate_parameters();

    /*
      Produces and returns the adjustment residuals.
    */
    Dictionary<const Point3D> residuals();
};

SimilarityTransformEstimator::Impl::Impl()
    : from_points(Dictionary<const Point3D>{}),
      to_points(Dictionary<const Point3D>{}),
      parameters(SimilarityTransformationParameters{}),
      parameter_variances(Eigen::Matrix<double, 7, 7>{}),
      is_computed(false)
{ }

SimilarityTransformEstimator::Impl::Impl(
    const Dictionary<const Point3D>& from,
    const Dictionary<const Point3D>& to)
    : from_points(std::move(from)),
      to_points(std::move(to)),
      parameters(SimilarityTransformationParameters{}),
      parameter_variances(Eigen::Matrix<double, 7, 7>{}),
      is_computed(false)
{
    if (!is_subset(from_points, to_points) ||
        !is_subset(to_points, from_points)) {
        throw ArgumentException(
            "Both sets of labeled points must be matched in the estimator.");
    }
}

void SimilarityTransformEstimator::Impl::add(const Label& label, Point3D from, Point3D to) noexcept
{
    // Remove the point in case it already exists
    remove(label);

    from_points.insert(DictEntry<const Point3D>(label, std::move(from)));
    to_points.insert(DictEntry<const Point3D>(label, std::move(to)));
    is_computed = false;
}

void SimilarityTransformEstimator::Impl::remove(const Label& label) noexcept
{
    if (from_points.count(label)) {
        from_points.erase(label);
    }
    if (to_points.count(label)) {
        to_points.erase(label);
    }
    is_computed = false;
}

std::vector<Label> SimilarityTransformEstimator::Impl::point_names() const noexcept
{
    return labels(from_points);
}

SimilarityTransformationParameters
SimilarityTransformEstimator::Impl::get_parameters()
{
    estimate_parameters();
    return parameters;
}

SimilarityTransformationParameters
SimilarityTransformEstimator::Impl::initial_parameter_approximations() const
{
    return HornsRegistrationEstimator(from_points, to_points).parameters();
}

Eigen::MatrixXd SimilarityTransformEstimator::Impl::first_design(
    const SimilarityTransformationParameters& approximates) const
{
    const auto names = point_names();
    auto A = Eigen::Matrix<double, Eigen::Dynamic, 7>(3 * names.size(), 7);

    for (auto i = 0U; i < names.size(); ++i) {
        const auto name = names[i];
        const auto from = from_points.at(name);

        const auto M = rotation_matrix(approximates.W, approximates.P, approximates.K);
        const auto rf = Eigen::Vector3d{ from.X, from.Y, from.Z };
        const auto partial_lambda = Eigen::Vector3d{M * rf};

        /* Partials of X^0 */
        A(3*i, 0) = 1;  // tx
        A(3*i, 1) = 0;  // ty
        A(3*i, 2) = 0;  // tz
        A(3*i, 3) = partial_x_omega(approximates, from);
        A(3*i, 4) = partial_x_phi(approximates, from);
        A(3*i, 5) = partial_x_kappa(approximates, from);
        A(3*i, 6) = partial_lambda(0);

        /* Partials of Y^0 */
        A(3*i+1, 0) = 0;  // tx
        A(3*i+1, 1) = 1;  // ty
        A(3*i+1, 2) = 0;  // tz
        A(3*i+1, 3) = partial_y_omega(approximates, from);
        A(3*i+1, 4) = partial_y_phi(approximates, from);
        A(3*i+1, 5) = partial_y_kappa(approximates, from);
        A(3*i+1, 6) = partial_lambda(1);

        /* Partials of Z^0 */
        A(3*i+2, 0) = 0;  // tx
        A(3*i+2, 1) = 0;  // ty
        A(3*i+2, 2) = 1;  // tz
        A(3*i+2, 3) = partial_z_omega(approximates, from);
        A(3*i+2, 4) = partial_z_phi(approximates, from);
        A(3*i+2, 5) = partial_z_kappa();
        A(3*i+2, 6) = partial_lambda(2);
    }
    return A;
}

Eigen::VectorXd SimilarityTransformEstimator::Impl::misclosure(
    const SimilarityTransformationParameters& parameters) const
{
    const auto names = point_names();
    auto W = Eigen::VectorXd(3 * names.size());

    for (auto i = 0U; i < names.size(); ++i) {
        const auto name = names.at(i);
        const auto from = from_points.at(name);
        const auto to = to_points.at(name);

        const auto M = rotation_matrix(parameters.W, parameters.P, parameters.K);
        const auto rf = Eigen::Vector3d{ from.X, from.Y, from.Z };
        const auto rt = Eigen::Vector3d{ to.X, to.Y, to.Z };
        const auto T = Eigen::Vector3d{
            parameters.Tx, parameters.Ty, parameters.Tz };

        const auto tmp = Eigen::Vector3d(parameters.scale * M * rf + T - rt);

        W(3*i)     = tmp(0);
        W(3*i + 1) = tmp(1);
        W(3*i + 2) = tmp(2);
    }
    return W;
}

void SimilarityTransformEstimator::Impl::estimate_parameters()
{
    static constexpr auto ITER_TOL = 20U;
    static constexpr auto TOLERANCE = 1e-9;

    if (is_computed) return;
    if (from_points.size() != to_points.size()) {
        throw TransformationException(
            "Unequal number of points to transform between to / from.");
    }
    if (from_points.size() < 3) {
        throw TransformationException(
            "Insufficient redundancy in solution to estimate transformation.");
    }

    auto params = initial_parameter_approximations();
    auto delta = Eigen::Matrix<double, 7, 1>(7, 1);

    // Init to vector of ones so we don't pass the tolerance test before
    // computing our solution.
    delta.setOnes(7, 1);

    auto N = Eigen::Matrix<double, 7, 7>(7, 7);

    for (auto i = 0U; delta.norm() > TOLERANCE; ++i) {
        if (i >= ITER_TOL) {
            throw TransformationException("Solution did not converge within acceptable limits.");
        }

        const auto A = first_design(params);
        const auto W = misclosure(params);

        N = A.transpose() * A;
        auto U = A.transpose() * W;

        delta = (N).ldlt().solve(U);
        params = SimilarityTransformationParameters {
            params.Tx - delta(0),
            params.Ty - delta(1),
            params.Tz - delta(2),
            params.W - delta(3),
            params.P - delta(4),
            params.K - delta(5),
            params.scale - delta(6)
        };
    }

    parameters = params;
    parameter_variances = N.inverse();
    is_computed = true;
}

Dictionary<const Point3D> SimilarityTransformEstimator::Impl::residuals()
{
    estimate_parameters();
    const auto st = SimilarityTransform(parameters);
    const auto names = point_names();

    auto ret = Dictionary<const Point3D>{};
    for (const auto& name : names) {
        const auto from = from_points.at(name);
        const auto to = to_points.at(name);

        const auto tpt = st.transform(from);
        ret.insert(
            DictEntry<const Point3D>(
                name,
                Point3D{tpt.X - to.X, tpt.Y - to.Y, tpt.Z - to.Z}));
    }

    return ret;
}

/*
  Public interface(s) for the SimilarityTransformEstimator class.
*/

SimilarityTransformEstimator::SimilarityTransformEstimator()
    : _pImpl(std::make_unique<SimilarityTransformEstimator::Impl>())
{ }

SimilarityTransformEstimator::SimilarityTransformEstimator(
    const Dictionary<const Point3D>& from_points,
    const Dictionary<const Point3D>& to_points)
    : _pImpl(std::make_unique<SimilarityTransformEstimator::Impl>(from_points, to_points))
{ }


SimilarityTransformEstimator::SimilarityTransformEstimator(
    SimilarityTransformEstimator&&) = default;

SimilarityTransformEstimator&
SimilarityTransformEstimator::operator=(SimilarityTransformEstimator&&) = default;

SimilarityTransformEstimator::~SimilarityTransformEstimator() = default;

void SimilarityTransformEstimator::add(
    const Label& label, Point3D from, Point3D to) noexcept
{
    _pImpl->add(label, std::move(from), std::move(to));
}

void SimilarityTransformEstimator::remove(const Label& label) noexcept
{
    _pImpl->remove(label);
}

std::vector<Label>
SimilarityTransformEstimator::point_names() const noexcept
{
    return _pImpl->point_names();
}

SimilarityTransformationParameters
SimilarityTransformEstimator::parameters()
{
    return _pImpl->get_parameters();
}

Dictionary<const Point3D> SimilarityTransformEstimator::residuals()
{
    return _pImpl->residuals();
}
