// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Implements SimilarityTransformationParameters.h
 */

#include "SimilarityTransformationParameters.h"

SimilarityTransformationParameters::SimilarityTransformationParameters()
    : SimilarityTransformationParameters(0, 0, 0, 0, 0, 0, 1.0)
{ }

SimilarityTransformationParameters::SimilarityTransformationParameters(
    double _Tx, double _Ty, double _Tz,
    double _Omega,  double _Phi,  double _Kappa, double _scale)
: Tx(std::move(_Tx)), Ty(std::move(_Ty)), Tz(std::move(_Tz)),
  W(std::move(_Omega)), P(std::move(_Phi)), K(std::move(_Kappa)),
  scale(std::move(_scale))
{ }

Dictionary<double> SimilarityTransformationParameters::to_dict() const noexcept
{
    auto ret = Dictionary<double>{};
    ret.insert(DictEntry<double>(Label("Tx [m]"), Tx));
    ret.insert(DictEntry<double>(Label("Ty [m]"), Ty));
    ret.insert(DictEntry<double>(Label("Tz [m]"), Tz));
    ret.insert(DictEntry<double>(Label("Omega [deg]"), W));
    ret.insert(DictEntry<double>(Label("Phi [deg]"), P));
    ret.insert(DictEntry<double>(Label("Kappa [deg]"), K));
    ret.insert(DictEntry<double>(Label("scale"), scale));
    return ret;
}
