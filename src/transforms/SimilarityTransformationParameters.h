// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Defines a struct and some basic members to hold similarity
  transformation parameters.
 */

#pragma once

#include "../misc/Dictionary.hpp"

struct SimilarityTransformationParameters
{
    double Tx;
    double Ty;
    double Tz;
    double W;
    double P;
    double K;
    double scale;

    /*
      Constructs a default set of transformation parameters.
      All parameters except scale are zero, and scale is set to 1.0.
    */
    explicit SimilarityTransformationParameters();

    /*
      Constructs a Similarity Transformation Parameter set from 7 values.

      @param _Tx - translation along the X axis.
      @param _Ty - translation along the Y axis.
      @param _Tz - translation along the Z axis.
      @param _W  - rotation about X axis in radians.
      @param _P  - rotation about Y axis in radians.
      @param _K  - rotation about Z axis in radians.
      @param _scale - the scale difference between two coordinate systems.
    */
    explicit SimilarityTransformationParameters(
        double _Tx, double _Ty, double _Tz,
        double _Omega,  double _Phi,  double _Kappa,
        double _scale);

    /*
      Converts the struct to a dictionary where the Dict labels have
      the same names as the struct members.
    */
    Dictionary<double> to_dict() const noexcept;
};
