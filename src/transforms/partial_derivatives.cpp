#include "partial_derivatives.h"

#include <cmath>

using std::cos;
using std::sin;

double partial_x_omega(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto k = params.K;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (Y * (cos(w) * sin(p) * cos(k) - sin(w) * sin(p)) +
                Z * (cos(w) * sin(k) + sin(w) * sin(p) * cos(k)));
}

double partial_x_phi(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto k = params.K;
    const auto X = point.X;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (-X * (sin(p) * cos(k)) +
                Y * (sin(w) * cos(p) * cos(k)) -
                Z * (cos(w) * cos(p) * cos(k)));
}

double partial_x_kappa(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto k = params.K;
    const auto X = point.X;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (-X * (cos(p) * sin(k)) +
                Y * (cos(w) * cos(k) - sin(w) * sin(p) * sin(k)) +
                Z * (sin(w) * cos(k) + cos(w) * sin(p) * sin(k)));
}

double partial_y_omega(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto k = params.K;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (Y * (-sin(w) * cos(k) - cos(w) * sin(p) * sin(k)) +
                Z * (cos(w) * cos(k) - sin(w) * sin(p) * sin(k)));
}

double partial_y_phi(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto k = params.K;
    const auto X = point.X;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (X * (sin(p) * sin(k)) -
                Y * (sin(w) * cos(p) * sin(k)) +
                Z * (cos(w) * cos(p) * sin(k)));
}

double partial_y_kappa(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto k = params.K;
    const auto X = point.X;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (-X * (cos(p) * cos(k)) -
                Y * (cos(w) * sin(k) + sin(w) * sin(p) * cos(k)) +
                Z * (cos(w) * sin(p) * cos(k) - sin(w) * sin(k)));
}

double partial_z_omega(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (-Y * (cos(w) * cos(p)) - Z * (sin(w) * cos(p)));
}

double partial_z_phi(
    const SimilarityTransformationParameters& params, const Point3D& point)
{
    const auto S = params.scale;
    const auto w = params.W;
    const auto p = params.P;
    const auto X = point.X;
    const auto Y = point.Y;
    const auto Z = point.Z;

    return S * (X * cos(p) + Y * sin(w) * sin(p) - Z * cos(w) * sin(p));
}

double partial_z_kappa() { return 0; }
