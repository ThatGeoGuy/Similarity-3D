// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
  Helper methods for constructing the first_design matrix for a 3D similarity
  transformation.
*/

#pragma once

#include "../points/Point3D.h"
#include "SimilarityTransformationParameters.h"


double partial_x_omega(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_x_phi(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_x_kappa(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_y_omega(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_y_phi(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_y_kappa(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_z_omega(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_z_phi(
    const SimilarityTransformationParameters&, const Point3D&);

double partial_z_kappa();
