// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Implements rotation.h
 */

#include <cmath>

#include "rotation.h"

using std::sin;
using std::cos;

EulerAngles::EulerAngles(double _omega, double _phi, double _kappa)
    : omega(std::move(_omega)), phi(std::move(_phi)), kappa(std::move(_kappa))
{ }

EulerAngles EulerAngles::from_unit_quaternion(
    double w, double xi, double yj, double zk) noexcept
{
    auto magnitude = sqrt(pow(w, 2) + pow(xi, 2) + pow(yj, 2) + pow(zk, 2));
    w /= magnitude;
    xi /= magnitude;
    yj /= magnitude;
    zk /= magnitude;

    auto omega = atan2(-2 * (yj * zk - w * xi),
                       pow(w, 2) - pow(xi, 2) - pow(yj, 2) + pow(zk, 2));
    auto phi = asin(2 * (xi * zk + w * yj));
    auto kappa = atan2(-2 * (xi * yj - w * zk),
                       pow(w, 2) + pow(xi, 2) - pow(yj, 2) - pow(zk, 2));
    return EulerAngles{omega, phi, kappa};
}

static Eigen::Matrix<double, 3, 3> R1(double omega) noexcept
{
    auto Rx = Eigen::Matrix<double, 3, 3>(3, 3);
    Rx << 1,          0,           0,
          0,  cos(omega), sin(omega),
          0, -sin(omega), cos(omega);
    return Rx;
}

static Eigen::Matrix<double, 3, 3> R2(double phi) noexcept
{
    auto Ry = Eigen::Matrix<double, 3, 3>(3, 3);
    Ry << cos(phi), 0, -sin(phi),
                 0, 1,         0,
          sin(phi), 0,  cos(phi);
    return Ry;
}

static Eigen::Matrix<double, 3, 3> R3(double kappa) noexcept
{
    auto Rz = Eigen::Matrix<double, 3, 3>(3, 3);
    Rz <<  cos(kappa), sin(kappa), 0,
          -sin(kappa), cos(kappa), 0,
                    0,          0, 1;
    return Rz;
}

Eigen::Matrix<double, 3, 3> rotation_matrix(double omega, double phi, double kappa) noexcept
{
    return R3(kappa) * R2(phi) * R1(omega);
}

Eigen::Matrix<double, 3, 3> rotation_matrix(
    double w, double xi, double yj, double zk) noexcept
{
    auto magnitude = sqrt(pow(w, 2) + pow(xi, 2) + pow(yj, 2) + pow(zk, 2));
    auto q0 = w / magnitude;
    auto q1 = xi / magnitude;
    auto q2 = yj / magnitude;
    auto q3 = zk / magnitude;

    auto M = Eigen::Matrix<double, 3, 3>(3, 3);
    M(0, 0) = pow(q0, 2) + pow(q1, 2) - pow(q2, 2) - pow(q3, 2);
    M(0, 1) = 2 * (q1 * q2 + q0 * q3);
    M(0, 2) = 2 * (q1 * q3 - q0 * q2);

    M(1, 0) = 2 * (q1 * q2 - q0 * q3);
    M(1, 1) = pow(q0, 2) - pow(q1, 2) + pow(q2, 2) - pow(q3, 2);
    M(1, 2) = 2 * (q2 * q3 + q0 * q1);

    M(2, 0) = 2 * (q1 * q3 + q0 * q2);
    M(2, 1) = 2 * (q2 * q3 - q0 * q1);
    M(2, 2) = pow(q0, 2) - pow(q1, 2) - pow(q2, 2) + pow(q3, 2);

    return M;
}
