// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Defines a function to create rotation matrices
 */

#pragma once

#include <Eigen/Dense>

/*
  Simple container struct to hold Euler angles.
*/
struct EulerAngles {
    double omega;
    double phi;
    double kappa;

    explicit EulerAngles(double, double, double);

    /*
      Produces a set of EulerAngles from quaternion components.

      @param w  - The real component of the quaternion.
      @param xi - The imaginary component of the quaternion.
      @param yj - The jmaginary component of the quaternion.
      @param zk - The kmaginary component of the quaternion.

      @returns a set of EulerAngles that define the same rotation as the unit
               quaternion.
    */
    static EulerAngles from_unit_quaternion(double w, double xi, double yj, double zk) noexcept;
};

/*
  Produces a rotation matrix rotating about the three cardinal axes.
  Uses the following formulation:

    M = R3(kappa) * R2(phi) * R1(omega)

  @param omega - first Euler angle to rotate about the X axis [rad].
  @param phi   - second Euler angle to rotate about the Y axis [rad].
  @param kappa - third Euler angle to rotate about the Z axis [rad].

  @returns the corresponding orthogonal rotation matrix rotating
           the three Euler angles.
*/
Eigen::Matrix<double, 3, 3> rotation_matrix(double omega, double phi, double kappa) noexcept;

/*
  Produces a rotation matrix from the components of a unit quaternion.

  @param w  - The real component of the quaternion.
  @param xi - The imaginary component of the quaternion.
  @param yj - The jmaginary component of the quaternion.
  @param zk - The kmaginary component of the quaternion.

  @returns the corresponding orthogonal rotation matrix for the given
           quaternion.
*/
Eigen::Matrix<double, 3, 3> rotation_matrix(double w, double xi, double yj, double zk) noexcept;
