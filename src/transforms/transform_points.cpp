// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "transform_points.h"

void transform_points(std::istream& sin,
                      const SimilarityTransformationParameters& parameters,
                      int width,
                      int precision)
{
    auto st = SimilarityTransform(parameters);
    double x, y, z;
    while (sin >> x >> y >> z) {
        const auto transformed_point = st.transform(Point3D{x, y, z});
        std::cout << std::setw(width)
                  << std::setprecision(precision)
                  << transformed_point
                  << std::endl;
    }
}
