// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

#include "SimilarityTransform.h"
#include "SimilarityTransformationParameters.h"

/*
  Helper procedure to read in points from an istream, transform them,
  and output to stdout.

  This function would appear to do a lot but with IO stuff like this it's
  easier to pack it all in one.

  @param sin        - the stream input.
  @param parameters - the similarity parameters to transform the point coordinate
                      by.
  @param width      - the width to print the points to.
  @param precision  - the precision to print the points to.
*/
void transform_points(
    std::istream&, const SimilarityTransformationParameters&, int, int);
