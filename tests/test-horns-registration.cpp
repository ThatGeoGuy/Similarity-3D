// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "catch.hpp"

#include "../src/transforms/SimilarityTransform.h"
#include "../src/transforms/SimilarityTransformationParameters.h"
#include "../src/transforms/HornsRegistrationEstimator.h"

SCENARIO("Using Horn's method to determine parameters.", "[transforms][horns]")
{
    GIVEN("A set of points") {
        auto points = Dictionary<const Point3D>{
            DictEntry<const Point3D>(Label("1"), Point3D{ 1, 0, 0 }),
            DictEntry<const Point3D>(Label("2"), Point3D{ 0, 1, 0 }),
            DictEntry<const Point3D>(Label("3"), Point3D{ 0, 0, 1 }),
            DictEntry<const Point3D>(Label("4"), Point3D{ 1, 1, 0 }),
            DictEntry<const Point3D>(Label("5"), Point3D{ 0, 1, 1 }),
            DictEntry<const Point3D>(Label("6"), Point3D{ 1, 0, 1 }),
            DictEntry<const Point3D>(Label("7"), Point3D{ 1, 1, 1 }),
            DictEntry<const Point3D>(Label("8"), Point3D{ 0, 0, 0 }),
        };

        WHEN ("Parameters are estimated using identical point sets.") {
            auto parameters = HornsRegistrationEstimator(points, points).parameters();

            THEN ("All the parameters should be almost zero, with unit scale.") {
                CHECK( parameters.Tx == Approx(0.0) );
                CHECK( parameters.Ty == Approx(0.0) );
                CHECK( parameters.Tz == Approx(0.0) );
                CHECK( parameters.W == Approx(0.0) );
                CHECK( parameters.P == Approx(0.0) );
                CHECK( parameters.K == Approx(0.0) );
                CHECK( parameters.scale == Approx(1.0) );
            }
        }
    }
}

SCENARIO("Estimates input transformation using HornsRegistrationEstimator.",
         "[transforms][horns]")
{
    GIVEN("A set of points and a transformation.")
    {
        auto points = Dictionary<const Point3D>{
            DictEntry<const Point3D>(Label("1"), Point3D{ 1, 0, 0 }),
            DictEntry<const Point3D>(Label("2"), Point3D{ 0, 1, 0 }),
            DictEntry<const Point3D>(Label("3"), Point3D{ 0, 0, 1 }),
            DictEntry<const Point3D>(Label("4"), Point3D{ 1, 1, 0 }),
            DictEntry<const Point3D>(Label("5"), Point3D{ 0, 1, 1 }),
            DictEntry<const Point3D>(Label("6"), Point3D{ 1, 0, 1 }),
            DictEntry<const Point3D>(Label("7"), Point3D{ 1, 1, 1 }),
            DictEntry<const Point3D>(Label("8"), Point3D{ 0, 0, 0 }),
        };

        auto inputs = SimilarityTransformationParameters{
            14, -23, 2, // Tx, Ty, Tz
            1.57898, 0, 2.222, // Rotations omega, phi, kappa
            1.3 // scale
        };

        auto st = SimilarityTransform(inputs);
        auto transformed_points = Dictionary<const Point3D>{};

        for (auto pair : points) {
            const auto name = pair.first;
            const auto pt = pair.second;

            transformed_points.insert(
                DictEntry<Point3D>(name, st.transform(pt)));
        }

        WHEN("Horns registration is applied to the two sets of points.")
        {
            auto hr = HornsRegistrationEstimator(points, transformed_points);

            THEN("The output parameters should be almost equivalent to"
                 "the input parameters.")
            {
                auto outputs = hr.parameters();

                CHECK( outputs.Tx == Approx(inputs.Tx).epsilon(0.05) );
                CHECK( outputs.Ty == Approx(inputs.Ty).epsilon(0.05) );
                CHECK( outputs.Tz == Approx(inputs.Tz).epsilon(0.05) );
                CHECK( outputs.W == Approx(inputs.W).epsilon(0.05) );
                CHECK( outputs.P == Approx(inputs.P).epsilon(0.05) );
                CHECK( outputs.K == Approx(inputs.K).epsilon(0.05) );
                CHECK( outputs.scale == Approx(inputs.scale).epsilon(0.05) );
            }
        }
    }
}

TEST_CASE("Horn's Estimator throws when given insufficient points.",
          "[transforms][horns]")
{
    auto hr = HornsRegistrationEstimator();
    REQUIRE_THROWS( hr.parameters() );
}

TEST_CASE("Horn's Estimator throws when given different numbers of points in "
          "left and right systems.",
          "[transforms][horns]")
{
    auto left = Dictionary<const Point3D>{
        DictEntry<const Point3D>(Label("1"), Point3D{1, 0, 0}),
        DictEntry<const Point3D>(Label("2"), Point3D{0, 1, 0}),
    };

    auto right = Dictionary<const Point3D>{
        DictEntry<const Point3D>(Label("a"), Point3D{1, 0, 0}),
        DictEntry<const Point3D>(Label("1"), Point3D{0, 1, 0}),
        DictEntry<const Point3D>(Label("2"), Point3D{1, 1, 0}),
    };

    REQUIRE_THROWS( HornsRegistrationEstimator(left, right) );
}
