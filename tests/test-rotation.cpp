// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "catch.hpp"

#include "../src/transforms/rotation.h"

TEST_CASE("Quaternion to Euler Angle conversions.", "[transforms][rotations][euler angles]")
{
    REQUIRE( EulerAngles(0, 0, 0).omega ==
             Approx(EulerAngles::from_unit_quaternion(1, 0, 0, 0).omega) );
    REQUIRE( EulerAngles(0, 0, 0).phi ==
             Approx(EulerAngles::from_unit_quaternion(1, 0, 0, 0).phi) );
    REQUIRE( EulerAngles(0, 0, 0).kappa ==
             Approx(EulerAngles::from_unit_quaternion(1, 0, 0, 0).kappa) );

    // The following magic numbers were converted from a reference Python module
    // I have created prior to this project.
    // Omega
    REQUIRE(
        EulerAngles::from_unit_quaternion(0.5, 0.2, -0.5, 0.2).omega ==
        Approx(1.5707963267948966));
    // Phi
    REQUIRE(
        EulerAngles::from_unit_quaternion(0.5, 0.2, -0.5, 0.2).phi ==
        Approx(-0.8097835725701666));
    // Kappa
    REQUIRE(
        EulerAngles::from_unit_quaternion(0.5, 0.2, -0.5, 0.2).kappa ==
        Approx(1.5707963267948966));
}

TEST_CASE("Zero rotation provides identity.", "[transforms][rotation]")
{
    REQUIRE( rotation_matrix(0, 0, 0).isApprox(Eigen::MatrixXd::Identity(3, 3)) );
    REQUIRE( rotation_matrix(1, 0, 0, 0).isApprox(Eigen::MatrixXd::Identity(3, 3)) );
}

TEST_CASE("Rotation matrix is correct size/shape.", "[transforms][rotation]")
{
    REQUIRE( rotation_matrix(0, 0, 0).rows() == 3 );
    REQUIRE( rotation_matrix(1, 0, 0, 0).rows() == 3 );
    REQUIRE( rotation_matrix(0, 0, 0).cols() == 3 );
    REQUIRE( rotation_matrix(1, 0, 0, 0).cols() == 3 );

    // Random omega-phi-kappa values
    REQUIRE( rotation_matrix(0, 40, 33).rows() == 3 );
    REQUIRE( rotation_matrix(0.5, 0.2, -0.5, 0.2).rows() == 3 );
    REQUIRE( rotation_matrix(0, 40, 33).cols() == 3 );
    REQUIRE( rotation_matrix(0.5, 0.2, -0.5, 0.2).cols() == 3 );
}

SCENARIO("Rotation matrix is orthogonal.", "[transforms][rotation]")
{
    GIVEN("A rotation matrix defined by zero Euler angles.") {
        auto M = rotation_matrix(0, 0, 0);
        THEN ("The matrix should be orthogonal.") {
            REQUIRE( M.transpose().isApprox(M.inverse()) );
        }
    }

    GIVEN("A rotation matrix defined by random Euler angles.") {
        auto M = rotation_matrix(30, 50, 22);
        THEN ("The matrix should be orthogonal.") {
            REQUIRE( M.transpose().isApprox(M.inverse()) );
        }
    }

    GIVEN("A rotation matrix defined by a pure-real unit-quaternion.") {
        auto M = rotation_matrix(1, 0, 0, 0);
        THEN ("The matrix should be orthogonal.") {
            REQUIRE( M.transpose().isApprox(M.inverse()) );
        }
    }

    GIVEN("A rotation matrix defined by a random, non-real unit-quaternion.") {
        auto M = rotation_matrix(0.5, 0.2, -0.5, 0.2);
        THEN ("The matrix should be orthogonal.") {
            REQUIRE( M.transpose().isApprox(M.inverse()) );
        }
    }
}
