// Similarity 3D
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "catch.hpp"

#include <algorithm>
#include <cmath>
#include <vector>

#include <Eigen/Dense>

#include "../src/transforms/SimilarityTransform.h"


SCENARIO("Inputs into the similarity transform should not be modified.",
         "[transforms][similarity]")
{
    GIVEN("A set of similarity transformation parameters")
    {
        auto params = SimilarityTransformationParameters{0, 0, 0, 0, 0, 0, 1.0};

        WHEN("These parameters are used to create a transform object.")
        {
            auto st = SimilarityTransform(params);

            THEN("The parameters in the class should be equivalent to the inputs.")
            {
                auto class_params = st.parameters();

                CHECK( class_params.Tx == Approx(params.Tx) );
                CHECK( class_params.Ty == Approx(params.Ty) );
                CHECK( class_params.Tz == Approx(params.Tz) );
                CHECK( class_params.W == Approx(params.W));
                CHECK( class_params.P == Approx(params.P));
                CHECK( class_params.K == Approx(params.K));
                CHECK( class_params.scale == Approx(params.scale) );
            }
        }
    }
}

SCENARIO("Identity transformation should not alter points.",
         "[transform][similarity]")
{
    GIVEN("A set of points")
    {
        auto points = std::vector<Point3D>{
            Point3D{ 1, 0, 0 },
            Point3D{ 0, 1, 0 },
            Point3D{ 0, 0, 1 },
            Point3D{ 1, 1, 0 },
            Point3D{ 0, 1, 1 },
            Point3D{ 1, 0, 1 },
            Point3D{ 1, 1, 1 },
            Point3D{ 0, 0, 0 },
        };

        WHEN("These points are transformed by the identity transformation.")
        {
            auto st = SimilarityTransform(SimilarityTransformationParameters{});

            for(const auto pt : points) {
                const auto tpt = st.transform(pt);

                THEN("The points should be equal before and after transformation.")
                {
                    CHECK( pt.X == Approx(tpt.X) );
                    CHECK( pt.Y == Approx(tpt.Y) );
                    CHECK( pt.Z == Approx(tpt.Z) );
                }
            }
        }
    }
}

SCENARIO("Transforming points individually vs all at once gives same result",
         "[transforms][similarity]")
{
    GIVEN("A set of points both as a matrix and individually.")
    {
        auto points_vector = std::vector<Point3D>{
            Point3D{ 1, 0, 0 },
            Point3D{ 0, 1, 0 },
            Point3D{ 0, 0, 1 },
            Point3D{ 1, 1, 0 },
            Point3D{ 0, 1, 1 },
            Point3D{ 1, 0, 1 },
            Point3D{ 1, 1, 1 },
            Point3D{ 0, 0, 0 },
        };

        auto points_matrix = Eigen::MatrixX3d(8, 3);
        points_matrix <<
            1, 0, 0,
            0, 1, 0,
            0, 0, 1,
            1, 1, 0,
            0, 1, 1,
            1, 0, 1,
            1, 1, 1,
            0, 0, 0;

        WHEN("The points are transformed by the same transformation parameters.")
        {
            const auto params = SimilarityTransformationParameters{
                1, 2, 2, 1.57, 0, 1.57, 1.0020
            };
            const auto st = SimilarityTransform(params);

            std::vector<Point3D> transformed_vector;
            transformed_vector.reserve(points_vector.size());

            std::transform(begin(points_vector),
                           end(points_vector),
                           back_inserter(transformed_vector),
                           [&st](auto point) {
                               return st.transform(point);
                           });

            auto transformed_matrix = st.transform_all(points_matrix);

            THEN("All points should be equal to their correspondent points.")
            {
                for (auto i = 0U; i < transformed_vector.size(); ++i) {
                    CHECK( transformed_vector.at(i).X ==
                             Approx(transformed_matrix(i, 0)) );
                    CHECK( transformed_vector.at(i).Y ==
                             Approx(transformed_matrix(i, 1)) );
                    CHECK( transformed_vector.at(i).Z ==
                             Approx(transformed_matrix(i, 2)) );
                }
            }
        }
    }
}

SCENARIO("Translation and rotation maintain scale",
         "[transforms][similarity][translation]")
{
    GIVEN("Two points.")
    {
        auto point1 = Point3D{1, 6, -3};
        auto point2 = Point3D{-2, -2, -2};

        WHEN("They are transformed with only translation.")
        {
            auto st = SimilarityTransform(
                SimilarityTransformationParameters{4, 4, 4, 0, 0, 0, 1.0});

            auto tpoint1 = st.transform(point1);
            auto tpoint2 = st.transform(point2);

            THEN("The distance between both points should be equal.")
            {
                auto dist = sqrt(pow(point1.X - point2.X, 2) +
                                 pow(point1.Y - point2.Y, 2) +
                                 pow(point1.Z - point2.Z, 2));

                auto tdist = sqrt(pow(tpoint1.X - tpoint2.X, 2) +
                                  pow(tpoint1.Y - tpoint2.Y, 2) +
                                  pow(tpoint1.Z - tpoint2.Z, 2));

                REQUIRE( dist == Approx(tdist) );
            }
        }

        WHEN("They are transformed with only rotation.")
        {
            auto st = SimilarityTransform(
                SimilarityTransformationParameters{0, 0, 0, 3.14, 1.5, 2, 1.0});

            auto tpoint1 = st.transform(point1);
            auto tpoint2 = st.transform(point2);

            THEN("The distance between both points should be equal.")
            {
                auto dist = sqrt(pow(point1.X - point2.X, 2) +
                                 pow(point1.Y - point2.Y, 2) +
                                 pow(point1.Z - point2.Z, 2));

                auto tdist = sqrt(pow(tpoint1.X - tpoint2.X, 2) +
                                  pow(tpoint1.Y - tpoint2.Y, 2) +
                                  pow(tpoint1.Z - tpoint2.Z, 2));

                REQUIRE( dist == Approx(tdist) );
            }
        }
    }
}

SCENARIO("Rotations about an axes do not change values on that axes.",
         "[transforms][similarity][rotation]")
{
    GIVEN("A set of points.")
    {
        auto points = std::vector<Point3D>{
            Point3D{ 1, 0, 0 },
            Point3D{ 0, 1, 0 },
            Point3D{ 0, 0, 1 },
            Point3D{ 1, 1, 0 },
            Point3D{ 0, 1, 1 },
            Point3D{ 1, 0, 1 },
            Point3D{ 1, 1, 1 },
            Point3D{ 0, 0, 0 },
        };

        WHEN("These points are (only) rotated around the X axis.")
        {
            auto st = SimilarityTransform(
                SimilarityTransformationParameters{0, 0, 0, 2.444, 0, 0, 1.0});

            for(const auto pt: points) {
                const auto tpt = st.transform(pt);

                THEN("The X values of all points should be the same.")
                {
                    REQUIRE( pt.X == Approx(tpt.X) );
                }
            }
        }

        WHEN("These points are (only) rotated around the Y axis.")
        {
            auto st = SimilarityTransform(
                SimilarityTransformationParameters{0, 0, 0, 0, 2.444, 0, 1.0});

            for(const auto pt: points) {
                const auto tpt = st.transform(pt);

                THEN("The Y values of all points should be the same.")
                {
                    REQUIRE( pt.Y == Approx(tpt.Y) );
                }
            }
        }

        WHEN("These points are (only) rotated around the Z axis.")
        {
            auto st = SimilarityTransform(
                SimilarityTransformationParameters{0, 0, 0, 0, 0, 2.444, 1.0});

            for(const auto pt: points) {
                const auto tpt = st.transform(pt);

                THEN("The Z values of all points should be the same.")
                {
                    REQUIRE( pt.Z == Approx(tpt.Z) );
                }
            }
        }
    }
}

SCENARIO("Scale factor changes distance between two points.",
         "[transforms][similarity][scale]")
{
    GIVEN("Two points.")
    {
        auto point1 = Point3D{1, 6, -3};
        auto point2 = Point3D{-2, -2, -2};

        WHEN("A scale transformation is applied.")
        {
            auto st = SimilarityTransform(
                SimilarityTransformationParameters{0, 0, 0, 0, 0, 0, 2.423213});

            auto tpoint1 = st.transform(point1);
            auto tpoint2 = st.transform(point2);

            THEN("The distances between the points should be different.")
            {
                auto dist = sqrt(pow(point1.X - point2.X, 2) +
                                 pow(point1.Y - point2.Y, 2) +
                                 pow(point1.Z - point2.Z, 2));

                auto tdist = sqrt(pow(tpoint1.X - tpoint2.X, 2) +
                                  pow(tpoint1.Y - tpoint2.Y, 2) +
                                  pow(tpoint1.Z - tpoint2.Z, 2));

                REQUIRE( dist != tdist );
            }
        }
    }
}
